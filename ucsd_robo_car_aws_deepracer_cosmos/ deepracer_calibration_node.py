import rclpy
from rclpy.node import Node
from sensor_msgs.msg import Image
from deepracer_interfaces_pkg.msg import ServoCtrlMsg
from rclpy.parameter import Parameter
from cv_bridge import CvBridge
import cv2
import numpy as np
import os.path

# Nodes in this program
DRC_NODE_NAME = 'deepracer_calibration_node'

# Nodes listening to rosparameters
LG_NODE_NAME = 'lane_guidance_node'
LD_NODE_NAME = 'lane_detection_node'

# Topics subscribed/published to in this program
CAMERA_TOPIC_NAME = '/camera_pkg/display_mjpeg'
THROTTLE_AND_STEERING_TOPIC_NAME = '/ctrl_pkg/servo_msg'

cv2.namedWindow('sliders')


def callback(x):
    pass

# Function to map [0:2000] ---> [-1,1]
def slider_to_normalized(slider_input):
    input_start = 0
    input_end = 2000
    output_start = -1
    output_end = 1
    normalized_output = output_start + (slider_input - input_start) * (
            (output_end - output_start) / (input_end - input_start))
    return normalized_output


# Creating default and max values for sliders
parameter_1_low = 0
parameter_1_high = 100

parameter_2_low = 0
parameter_2_high = 1000

parameter_3_low = 0
parameter_3_high = 300

steer_left = 0
steer_straight = 1000
steer_right = 2000

steer_sensitivity_max = 100
steer_sensitivity_default = 100

throttle_reverse = 0
throttle_neutral = 1100
throttle_forward = 2000

zero_error_throttle_mode = 0
error_throttle_mode = 1

# Creating sliders
cv2.createTrackbar('parameter_1', 'sliders', parameter_1_low, parameter_1_high, callback)
cv2.createTrackbar('parameter_2', 'sliders', parameter_2_low, parameter_2_high, callback)
cv2.createTrackbar('parameter_3', 'sliders', parameter_3_low, parameter_3_high, callback)

cv2.createTrackbar('Steering_sensitivity', 'sliders', steer_sensitivity_default, steer_sensitivity_max, callback)
cv2.createTrackbar('Steering_value', 'sliders', steer_straight, steer_right, callback)
cv2.createTrackbar('Throttle_mode', 'sliders', zero_error_throttle_mode, error_throttle_mode, callback)
cv2.createTrackbar('Throttle_value', 'sliders', throttle_neutral, throttle_forward, callback)


class DeepRacerCalibration(Node):
    def __init__(self):
        # Initializing DeepRacerCalibration class
        super().__init__(DRC_NODE_NAME)
        self.throttle_and_steering_publisher = self.create_publisher(ServoCtrlMsg, THROTTLE_AND_STEERING_TOPIC_NAME, 10)
        self.throttle_and_steering_publisher
        self.throttle_and_steering_values = ServoCtrlMsg()
        self.camera_subscriber = self.create_subscription(Image, CAMERA_TOPIC_NAME, self.live_calibration_values, 10)
        self.camera_subscriber
        self.bridge = CvBridge()

        # declare parameters
        self.declare_parameter('parameter_1')
        self.declare_parameter('parameter_2')
        self.declare_parameter('parameter_3')
        self.declare_parameter('Steering_sensitivity')
        self.declare_parameter('zero_error_throttle')
        self.declare_parameter('error_throttle')

        # Setting default values for actuators
        self.zero_error_throttle = 0.55
        self.error_throttle = 0.5

        # Setting error threshold (ranges from [0.0:1.0]) for lines detected, 0.1 ---> 10%
        self.error_threshold = 0.1


    def live_calibration_values(self, data):
        # get trackbar positions
        parameter_1 = cv2.getTrackbarPos('parameter_1', 'sliders')
        parameter_2 = cv2.getTrackbarPos('parameter_2', 'sliders')
        parameter_3 = cv2.getTrackbarPos('parameter_3', 'sliders')
        steer_input = cv2.getTrackbarPos('Steering_value', 'sliders')
        Steering_sensitivity = float(cv2.getTrackbarPos('Steering_sensitivity', 'sliders')/100)
        Throttle_mode = cv2.getTrackbarPos('Throttle_mode', 'sliders')
        throttle_input = cv2.getTrackbarPos('Throttle_value', 'sliders')

        # Logic for setting throttle values based on throttle mode
        if Throttle_mode == 0:
            self.zero_error_throttle = slider_to_normalized(throttle_input)
        else:
            self.error_throttle = slider_to_normalized(throttle_input)

        # Publish throttle and steering values
        self.throttle_and_steering_values.angle = Steering_sensitivity*slider_to_normalized(steer_input)
        self.throttle_and_steering_values.throttle = slider_to_normalized(throttle_input)
        self.throttle_and_steering_publisher.publish(self.throttle_and_steering_values)

        # Image processing from slider values
        frame = self.bridge.imgmsg_to_cv2(data)
        
        # plotting results
        cv2.imshow('frame', frame)
        cv2.waitKey(1)

        # Write parameters to yaml file for storage
        config_path = '/home/deepracer/projects/deepracer_ws/src/ucsd_robo_car_aws_deepracer_cosmos/config/deepracer_config.yaml'
        f = open(config_path, "w")
        f.write(
            f"{LD_NODE_NAME}: \n"
            f"  ros__parameters: \n"
            f"    parameter_1 : {parameter_1} \n"
            f"    parameter_2 : {parameter_2} \n"
            f"    parameter_3 : {parameter_3} \n"
            f"{LG_NODE_NAME}: \n"
            f"  ros__parameters: \n"
            f"    steering_sensitivity : {Steering_sensitivity} \n"
            f"    no_error_throttle : {self.zero_error_throttle} \n"
            f"    error_throttle : {self.error_throttle} \n"
        )
        f.close()


def main(args=None):
    rclpy.init(args=args)
    DRC_publisher = DeepRacerCalibration()
    try:
        rclpy.spin(DRC_publisher)
        DRC_publisher.destroy_node()
        rclpy.shutdown()
    except KeyboardInterrupt:
        print(f"\nShutting down {DRC_NODE_NAME}...")
        DRC_publisher.throttle_and_steering_values.angle = 0.0
        DRC_publisher.throttle_and_steering_values.throttle = 0.0
        DRC_publisher.throttle_and_steering_publisher.publish(DRC_publisher.throttle_and_steering_values)
        cv2.destroyAllWindows()
        DRC_publisher.destroy_node()
        rclpy.shutdown()
        print(f"{DRC_NODE_NAME} shut down successfully.")


if __name__ == '__main__':
    main()
